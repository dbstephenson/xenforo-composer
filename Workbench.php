<?php

use Symfony\Component\Finder\Finder;

class Composer_Workbench {

	/**
	 * Load the workbench vendor auto-load files.
	 *
	 * @param  string  $path
	 * @param  \Symfony\Component\Finder\Finder  $finder
	 * @return void
	 */
	public static function start($path, Finder $finder = null)
	{
		$finder = $finder ?: new Finder;

		$autoloads = $finder->in($path)->files()->name('autoload.php')->depth('<= 3')->followLinks();

		foreach ($autoloads as $file)
		{
			require_once $file->getRealPath();
		}
	}

}
